module kafka-go

go 1.18

require (
	github.com/confluentinc/confluent-kafka-go/v2 v2.0.2
	github.com/google/uuid v1.3.0
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/go-chi/chi/v5 v5.0.8 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
