package api

import (
	"encoding/json"
	"fmt"
	orderController "kafka-go/controllers"
	"kafka-go/models"
	"net/http"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
)

func GetAll(w http.ResponseWriter, r *http.Request) {
	orders := orderController.GetAll()
	ordersIDs := []string{}
	for _, order := range orders {
		ordersIDs = append(ordersIDs, order.ID.String())
	}
	w.Write([]byte(fmt.Sprintf("all orders: %s", ordersIDs)))
}

func Place(w http.ResponseWriter, r *http.Request) {
	var order models.Order
	order.ID = uuid.New()
	var err error
	if err = json.NewDecoder(r.Body).Decode(&order); err != nil {
		log.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	log.WithField("order", order).Info("received new order")
	orderController.Place(order)
	w.Write([]byte(fmt.Sprintf("order placed: %s", r.URL.Path)))
}

func LiveApi(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("Live API: %s", r.URL.Path)))
}
