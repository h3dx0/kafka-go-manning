package eventhandlers

import (
	"encoding/json"
	"fmt"
	"kafka-go/events"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

type Handler struct {
	Topic string
	Event events.Event
}
type EventHandler interface {
	Send(topic string, event events.Event) error
}

func (eh Handler) New(topic string, event events.Event) Handler {
	return Handler{
		Topic: topic,
		Event: event,
	}
}

func (eh Handler) Send() error {
	producer, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "localhost:9092"})
	if err != nil {
		fmt.Println("Failed to create producer:", err)
		return err
	}

	defer producer.Close()
	var value []byte
	if value, err = json.Marshal(eh.Event); err != nil {
		return err
	}
	// Produce a message to the "my-topic" topic
	message := &kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &eh.Topic, Partition: kafka.PartitionAny},
		Value:          value,
	}

	// Asynchronously produce a message, delivery report handler will be called once the message has been successfully
	// delivered or has failed permanently
	producer.Produce(message, nil)

	// Wait for any outstanding messages to be delivered and delivery report callbacks to be received.
	producer.Flush(15 * 1000)
	return nil
}
