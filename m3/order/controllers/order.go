package controllers

import (
	"kafka-go/config"
	eventhandlers "kafka-go/event_handlers"
	"kafka-go/events"
	"kafka-go/models"

	"time"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
)

func GetAll() map[string]models.Order {
	orders := map[string]models.Order{
		"1": models.Order{
			ID: uuid.New(),
			Products: []models.Product{
				models.Product{
					Name:        "Product 1",
					ProductCode: "P1",
					Quantity:    1,
				},
			},
			Customer: models.Customer{
				FirstName:    "John",
				LastName:     "Doe",
				EmailAddress: "pepe@local.com",
				ShippingAddress: models.Address{
					Line1:      "123 Main St",
					Line2:      "Apt 1",
					City:       "Anytown",
					State:      "CA",
					PostalCode: "12345",
				},
			},
		},
	}
	return orders
}

func Place(order models.Order) {

	var event = events.OrderReceived{
		EventBase: events.BaseEvent{
			EventID:        uuid.New(),
			EventTimestamp: time.Now(),
		},
		EventBody: order,
	}
	var eventHandler eventhandlers.Handler
	eventHandler = eventHandler.New(config.OrdersReceivedTopic, event)
	if err := eventHandler.Send(); err != nil {
		log.WithField("orderID", order.ID).Error(err.Error())
	} else {
		log.WithField("event", event).Info("published event")
	}
}
