package main

import (
	"fmt"
	"kafka-go/api"
	"net/http"

	chi "github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	fmt.Println("Starging the application...")
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", api.LiveApi)

	r.Route("/orders", func(r chi.Router) {
		r.Get("/", api.GetAll)
		r.Post("/", api.Place)
	})
	err := http.ListenAndServe(":8080", r)
	if err != nil {
		fmt.Println("Error starting the server")
		fmt.Println(err)
		panic(err)
	}
}
