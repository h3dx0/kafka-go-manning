package main

import (
	"fmt"
	"kafka-go/api"
	"net/http"
)

func ordersHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		fmt.Println("GET request received, sendig a message to the topic 'OrderReceived'")
		api.PlaceOrder()
	case "POST":
		fmt.Println("POST request received")
	default:
		fmt.Println("Unsupported request received")
	}
}

func main() {
	fmt.Println("Starging the application...")
	http.HandleFunc("/orders", ordersHandler)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("Error starting the server")
		fmt.Println(err)
		panic(err)
	}
}
