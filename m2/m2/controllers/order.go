package controllers

import (
	"kafka-go/config"
	eventhandlers "kafka-go/event_handlers"
	"kafka-go/events"
	"kafka-go/models"

	"time"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
)

func Place() {
	order := models.Order{
		ID: uuid.New(),
	}
	var event = events.OrderReceived{
		EventBase: events.BaseEvent{
			EventID:        uuid.New(),
			EventTimestamp: time.Now(),
		},
		EventBody: order,
	}
	eventHandler := eventhandlers.Handler{
		Topic: config.OrdersReceivedTopic,
		Event: event,
	}

	if err := eventHandler.Send(); err != nil {
		log.WithField("orderID", order.ID).Error(err.Error())
	} else {
		log.WithField("event", event).Info("published event")
	}
}
